# WARP

  

The CI/CD automatically provisions Kafka Cluster builds in the production server.

NOTE: Running the CI/CD -> Stopping curent instance and build a new one.

## Current Kafka Infrastructure
Currently, this is the mapping between service <> exposed port provisioned in this repo is

- 1 Zookeeper Instance
- 3 Kafka Brokers
- Kafka Manager
- Kafka Schema Registry
- Kafka Schema Registry UI

Detailed status about each deployment can be accessed through *Portainer*.

### Server Side Set-Up
- Please add private SSH key in *variables* section of CI/CD settings
- Create `.env` file from `example.env` file
- Create `warp_deployment.sh` file in the CI/CD target folder. The example script is below.

```
echo "WARP PROVISIONING IS STARTING....."

# Move to working directory
cd /home/admin/docker_utils/warp

# Pull latest version
git checkout master
git pull 'git@gitlab.cs.ui.ac.id:csui-datastream/warp.git' master

# Stack Provisioning
bash run_kafka_env.sh

echo "WARP PROVISIONING DONE....."
```
Author: Favian Kharisma Hazman