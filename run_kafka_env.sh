#!bin/bash
docker-compose up -d
docker run -d --name=warp-kafka-manager --network=warp-net -it --rm  -p 9001:9000 -e ZK_HOSTS="warp_zk-1_1:2181" -e APPLICATION_SECRET=letmein sheepkiller/kafka-manager